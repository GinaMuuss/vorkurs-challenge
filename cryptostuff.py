import string
import random
import secrets

from cryptography.hazmat.primitives.padding import PKCS7
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


class Cryptor:
    def __init__(self, key):
        super().__init__()
        self.key = key if isinstance(key, bytes) else key.encode()
        assert len(key) == 16, 'key must be 16 bytes'

    def _pad_message(self, msg):
        padder = PKCS7(128).padder()
        return padder.update(msg) + padder.finalize()

    def _unpad_msg(self, msg):
        padder = PKCS7(128).unpadder()
        return padder.update(msg) + padder.finalize()

    def encrypt(self, msg):
        msg = msg if isinstance(msg, bytes) else msg.encode()
        padded_msg = self._pad_message(msg)
        iv = secrets.token_bytes(16)
        cipher = Cipher(algorithms.AES(self.key), modes.CBC(iv)).encryptor()
        ciphertext = cipher.update(padded_msg) + cipher.finalize()
        return (iv + ciphertext).hex()

    def decrypt(self, ciphertext_iv):
        ciphertext_iv = bytes.fromhex(ciphertext_iv) if isinstance(ciphertext_iv, str) else ciphertext_iv
        iv = ciphertext_iv[:16]
        ciphertext = ciphertext_iv[16:]
        cipher = Cipher(algorithms.AES(self.key), modes.CBC(iv)).decryptor()
        padded_msg = cipher.update(ciphertext) + cipher.finalize()
        return self._unpad_msg(padded_msg).decode()


def split_and_encode_key(key, list_len=100):
    assert len(key) == 16 and all(c in string.ascii_lowercase for c in key)

    first, second = key[:8], key[8:]

    fenc = encode(first, list_len)
    senc = encode(second, list_len)
    return {'first': fenc, 'second': senc}


def encode(part, list_len):
    def get_freq(l, i):
        freq_map = {c: 0 for c in string.ascii_lowercase}
        for e in l:
            freq_map[e[i]] += 1
        return freq_map
        
    def is_max(m, c):
        as_list = sorted(m.items(), key=lambda t: t[1], reverse=True)
        return as_list[0][0] == c and as_list[0][1] != as_list[1][1]
    

    output = [[random.choice(string.ascii_lowercase) for _ in range(8)] for _ in range(list_len)]

    for c_idx, target_char in enumerate(part):
        freq_map = get_freq(output, c_idx)
        while not is_max(freq_map, target_char):
            while True:
                ridx = random.randint(0, list_len - 1)
                if (lchar := output[ridx][c_idx]) != target_char:
                    output[ridx][c_idx] = target_char
                    freq_map[target_char] += 1
                    freq_map[lchar] -= 1
                    break
    
    return [''.join(sl) for sl in output]


